"""
Napisite gramatiku s uzorcima tagova i istrenirjte
chunk parser za koordinirane imenicke fraze
(npr. July/NNP, and/CC, August/NNP, all/DT, your/PRP$,
managers/NNS, and/CC, supervisors/NNS", "company/NN,
courts/NNS, and/CC, adjudicators/NNS)
"""
import nltk

sentence = [
	("July", "NNP"),
	("and", "CC"),
	("August", "NNP"),
	("all", "DT"),
	("your", "PRP$"),
	("managers", "NNS"),
	("and", "CC"),
	("supervisors", "NNS"),
	("company", "NN"),
	("courts", "NNS"),
	("and", "CC"),
	("adjudicators", "NNS")
]
grammar = """NP: {<NNP><CC><NNP>}
{(<DT><PRP\$>)?<NN>?<NNS><CC><NNS>}"""

cp = nltk.RegexpParser(grammar)
result = cp.parse(sentence)
print(result)
