'''
Napravite kombinaciju tagera na istim setovima podataka, gdje je
unigramskom tageru backoff tager po deafaultu, a bigramskom tageru
backoff unigramski tager
'''

import nltk
from nltk.corpus import brown

brown_tagged_sents = brown.tagged_sents(categories='fiction')

limit = int(len(brown_tagged_sents) * 0.9)

train = brown_tagged_sents[:limit]
test = brown_tagged_sents[limit:]    

default_tagger = nltk.DefaultTagger('NN')
unigram_tagger = nltk.UnigramTagger(train, backoff=default_tagger)
bigram_tagger = nltk.BigramTagger(train, backoff=unigram_tagger)

print(bigram_tagger.evaluate(test))
