'''
U Brownovom korpusu, kategoriji fiction, pronađite najcesci tag
'''

import nltk
from nltk.corpus import brown

tags = [tag for (word, tag) in brown.tagged_words(categories='fiction')]
tags_max = nltk.FreqDist(tags).max()

print("Najcesci tag za fiction:", tags_max)
