'''
U Brownovom korpusu, kategoriji fiction, odvojite podatke za treniranje
modela i za testiranje u omjeru 9:1
'''

from nltk.corpus import brown

brown_tagged_sents = brown.tagged_sents(categories='fiction')
limit = int(len(brown_tagged_sents) * 0.9)

train = brown_tagged_sents[:limit]
test = brown_tagged_sents[limit:]
