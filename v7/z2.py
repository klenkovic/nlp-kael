'''
“Istrenirajte” DefaultTagger na recenici: “Once upon a time in my
younger years and in the dawn of this century I wrote”, a kao parametar
navedite najcesci tag kojeg ste pronasli u prethodnom koraku
'''

import nltk

rec = "Once upon a time in my younger years and in the dawn of this century I wrote"

default_tagger = nltk.DefaultTagger('NN')
print(default_tagger.tag(rec))
