def document_features(document):

    document_words = set(document)
    features = {}

    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)

    return features


import nltk
import random


comm = []

for i in range(20):
    pFileName = "pos/p" + str(i+1) + ".txt"
    nFileName = "neg/n" + str(i+1) + ".txt"
    
    p = open(pFileName, "r")
    n = open(nFileName, "r")

    for j, k in zip(p, n):
        comm = comm + [(j[:-1], 'pos')]
        comm = comm + [(k[:-1], 'neg')]
    
random.shuffle(comm)

comments = []
word_features = []

for i in comm:
    tokens = nltk.word_tokenize(i[0])
    comments = comments + [(tokens, i[1])]
    word_features = word_features + tokens

for i in range(len(word_features)):
    word_features[i] = word_features[i].lower()

word_features = list(set(word_features))

featuresets = [(document_features(d), c) for (d,c) in comments]

omjer = 0.8
tr_omjer = int(len(featuresets) * 0.8)
te_omjer = len(featuresets) - tr_omjer

train_set, test_set = featuresets[tr_omjer : ], featuresets[ : -te_omjer]

classifier = nltk.NaiveBayesClassifier.train(train_set)

print nltk.classify.accuracy(classifier, test_set)

print classifier.show_most_informative_features(5)
