Zadatak
=======

* Napravite skriptu koja će skinuti sadržaj s neke stranice te ga spremiti u datoteku sadrzaj.txt. (0,25 boda)

* Napravite skriptu koja će skinuti sve linkove s neke stranice i spremiti ih u datoteku. (0,25 bodova)

* Iz datoteke sadrzaj.txt izvadite sve html tagove i interpunkcijske znakove i sačuvajte takav sadržaj u datoteku sadrzaj_procisceni.txt (0,5 bodova)
