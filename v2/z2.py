'''
http://www.inf.uniri.hr/hr/

* Napravite skriptu koja će skinuti sve linkove 
s neke stranice i spremiti ih u datoteku. (0,25 bodova)

Python 3

'''

from lxml import html

url = "http://www.inf.uniri.hr/hr/"
html_content = open('sadrzaj.txt').read()

output = open('linkovi.txt', 'w')

links = list(html.iterlinks(html_content))

#print(links)

for l in links:
	if l[1]=='href':
		output.write(l[2]+'\n')
