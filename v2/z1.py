'''
http://www.inf.uniri.hr/hr/

Napravite skriptu koja ce skinuti sadrzaj s neke stranice
te ga spremiti u datoteku sadrzaj.txt. (0,25 boda)

Python 2.7

'''

import urllib

url = "http://www.inf.uniri.hr/hr/"
html_content = urllib.urlopen(url).read()

output = open('sadrzaj.txt', 'w')

output.write(html_content)
