'''
http://www.inf.uniri.hr/hr/

* Napravite skriptu koja će skinuti sve linkove 
s neke stranice i spremiti ih u datoteku. (0,25 bodova)

Python 3

'''

#from lxml import html

import bs4
import string

exclude = set(string.punctuation)
html_content = open('sadrzaj.txt').read()

output = open('sadrzaj_procisceni.txt', 'w')

#links = list(html.iterlinks(html_content))

soup = bs4.BeautifulSoup(html_content)
txt = soup.get_text()
txt = ''.join(ch for ch in txt if ch not in exclude)

#print(links)

#for l in links:
#	if l[1]=='href':
#		output.write(l[2]+'\n')

output.write(txt)
