def gender_features(word):
    
    return {'last_letter': word[-1]}


import nltk
import random
from nltk import MaxentClassifier

m = open("m.txt", "r")
z = open("z.txt", "r")

imena = []

for i, j in zip(m, z):
    imena = imena + [(i[:-1], "male")]
    imena = imena + [(j[:-1], "female")]

random.shuffle(imena)
omjer_train = 0.7
duljina = len(imena)
tr_omjer = int(omjer_train * duljina)
te_omjer = duljina - tr_omjer

znacajke = [(gender_features(n), g) for (n, g) in imena]

train_set, test_set = znacajke[tr_omjer:], znacajke[:-te_omjer]

classifier = nltk.NaiveBayesClassifier.train(train_set) # Bayes
me_classifier = MaxentClassifier.train(train_set) # Max entropija
classifier_tree = nltk.DecisionTreeClassifier.train(train_set) # Stablo

print("Ukupan accuracy za Bayes: ", nltk.classify.accuracy(classifier, test_set))
print("Ukupan accuracy za Max entropiju: ", nltk.classify.accuracy(me_classifier, test_set))
print("Ukupan accuracy za Stablo: ", nltk.classify.accuracy(classifier_tree, test_set))

print("Najinformativnije za Bayes: ", classifier.show_most_informative_features(10))
print("Najinformativnije za Max entropiju: ", me_classifier.show_most_informative_features(10))

# dodatno
print("\n- - - - - - DODATNO - - - - - -")
print("Bay: Kristijan -> ", classifier.classify(gender_features('Kristijan')))
print("Bay: Kristina -> ", classifier.classify(gender_features('Kristina')))
print("Bay: Mario -> ", classifier.classify(gender_features('Mario')))
print("Bay: Marija -> ", classifier.classify(gender_features('Marija')))

print("Ent: Kristijan -> ", me_classifier.classify(gender_features('Kristijan')))
print("Ent: Kristina -> ", me_classifier.classify(gender_features('Kristina')))
print("Ent: Mario -> ", me_classifier.classify(gender_features('Mario')))
print("Ent: Marija -> ", me_classifier.classify(gender_features('Marija')))

print("Tree: Kristijan -> ", classifier_tree.classify(gender_features('Kristijan')))
print("Tree: Kristina -> ", classifier_tree.classify(gender_features('Kristina')))
print("Tree: Mario -> ", classifier_tree.classify(gender_features('Mario')))
print("Tree: Marija -> ", classifier_tree.classify(gender_features('Marija')))

m.close()
z.close()
