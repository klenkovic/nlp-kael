import nltk
from bs4 import BeautifulSoup
import urllib3
import re


url = "http://www.inf.uniri.hr/hr/"

r = urllib3.PoolManager().request("GET", url)

soup = BeautifulSoup(r.data)
txt = soup.get_text()

tokeni = nltk.word_tokenize(txt)

wsj = sorted(set(tokeni))
fd = nltk.FreqDist(vs for word in wsj for vs in re.findall(r'[aeiou]{2}',word))
print([i for i in fd.items()])
